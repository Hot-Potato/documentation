# Trouble Codes

## What is a trouble code?

A trouble code is a short string that can be included in notifications that provides a reference you can look up in your preferred documentation system. Hot Potato includes a few of these by default that link to the application documentation in the event of a problem with Hot Potato itself.

Trouble codes are expected to be short and made up of two parts, the first being a string of characters which indicate a particular customer, environment or system, followed by digits. For example, you may have generic trouble codes (for things like disk space) which start with GENERIC and are numbered 000 through 999, giving you codes such as GENERIC003, GENERIC426 and GENERIC047.

## List of trouble codes built into Hot Potato

* [HOTPOTATO001 - Inability to send notifications](hotpotato001.md)
* [HOTPOTATO002 - Not receiving heartbeats](hotpotato002.md)
