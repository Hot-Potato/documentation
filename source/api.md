# API

API endpoint for versions 0.8.0+ is: `/api/servers/{API_VERSION}`.

All endpoints currently require authorization in the form of an API key. This is
returned when you add a server to hotpotato.

## V2

This version of the API is used from Hot Potato v0.8.0 onwards.

### Authentication

Monitoring servers get API keys generated for them once they are added to the Hot Potato database. Servers use these to authenticate themselves when interacting with the REST API.

If an API endpoint requires authentication, the monitoring server must include an `Authorization` HTTP header, using a custom `Apikey` authentication scheme, with the API key itself as the provided credentials.

```
Authorization: Apikey 1a2b3c4d5e6f...
```

If the monitoring server fails to provide a valid API key in a request that requires one, they will receive an API standard response with an error code of `401` (unauthorised), and an error message providing details in the `message` field.

```eval_rst
.. http:post:: /heartbeats/create

   Send a heartbeat to a server

   **Example request**

   .. sourcecode:: http

      POST /heartbeats/create HTTP/1.1
      Authorization: apikey s7f6sa8d7g6df8gh76dfsg876dfsg876sdf78g6ds7gf6ds8f67d7s8g8934qysdgkjh389dsf43tog4kdj3lsh3jskcm

   **Example response**

   .. sourcecode:: http

      HTTP/1.1 201 Created
      Content-Type: text/javascript

      {
         "success": true,
         "code": 201,
         "message": null,
         "data": {
            "id": 123345,
            "server_id": 12,
            "recieved_dt": 2018-07-10 02:08:57.717142
         }
      }

   :reqheader Authorization: required, currently only apikey allowed

   :statuscode 201: received successfully
   :statuscode 400: bad request
```

```eval_rst
.. http:get:: /notifications/get/(int:notification_id)

   Get a notification as JSON

   **Example request**

   .. sourcecode:: http

      GET /notifications/get/15 HTTP/1.1
      Authorization: apikey s7f6sa8d7g6df8gh76dfsg876dfsg876sdf78g6ds7gf6ds8f67d7s8g8934qysdgkjh389dsf43tog4kdj3lsh3jskcm

   **Example response**

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: text/javascript

      {
         "success": true,
         "code": 200,
         "message": null,
         "data": {
            "hostname": "host",
            "servicename": "",
            "troublecode": "",
            "displayname": "",
            "output": "",
            "notificationtype": ""
         }
      }

   :param int notification_id: notification id

   :reqheader Authorization: required, currently only apikey allowed

   :status 200: notification retrieved successfully
   :status 404: notification not found
   :status 415: invalid message direction
```

```eval_rst
.. http:post:: /notifications/send

   Create a notification and send it to the on-call person.

   **Example request**

   .. sourcecode:: http

      POST /notifications/send HTTP/1.1
      Authorization: apikey s7f6sa8d7g6df8gh76dfsg876dfsg876sdf78g6ds7gf6ds8f67d7s8g8934qysdgkjh389dsf43tog4kdj3lsh3jskcm

   **Example response**

   .. sourcecode:: http

      HTTP/1.1 201 Created
      Content-Type: text/javascript

      {
         "success": true,
         "code": 200,
         "message": null,
         "data": {
            "tenant_id": 12,
            "user_id": 1,
            "notif_type": "",
            "server_id": 13,
            "server_name": "myserver",
            "trouble_code": "BAD01",
            "hostname": "myhost",
            "display_name": "My host",
            "service_name": "Postgres",
            "state": "Down",
            "output": "...",
            "timestamp": "2018-07-10 02:08:57.717142"
         }
      }

   :reqjson string notif_type: the type of notification
   :reqjson int trouble_code: the issue code
   :reqjson string hostname: the server's hostname
   :reqjson string display_name: the server's display name
   :reqjson string service_name: the service name
   :reqjson string state: a
   :reqjson string output: a

   :status 201: sent successfully
```

## V1

```deprecated:: 7.0

```

This version of the API is used by all versions of Hot Potato up to v0.7.0. Hot Potato v0.8.0 onwards are backwards compatible with this protocol, but the endpoint has been relocated to allow additional REST APIs to be accessible.

To use the version 1 monitoring server API on Hot Potato version 0.7.0 or earlier, configure the Hot Potato client on your monitoring server with an API URL similar to the following: `https://hotpotato.example.com/api/v1`.

### Authentication

Monitoring servers get API keys generated for them once they are added to the Hot Potato database. Servers use these to authenticate themselves when interacting with the REST API.

If an API endpoint requires authentication, the monitoring server must include an `apikey` JSON field, with the API key itself as the provided credentials.

```json
{
  "apikey": "1a2c3c4d5e6f..."
}
```

If the monitoring server fails to provide a valid API key in a request that requires one, they will receive an API standard response with an error code of `403` (unauthorised), and an error message providing details as the output.

### Notifications

API endpoints for getting and sending notifications.

```eval_rst
.. http:post:: /check/notification

   Check if a notification exists

   **Example response**

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: text/plain

      true

   :reqjson int id: ID of the notification

   :reqheader Authorization: required, currently only apikey allowed

   :statuscode 200: Notification exists
   :statuscode 404: Notification doesn't exist
```

```eval_rst
.. http:post:: /add/notification

   Create a notification, and send it to the on-call person.
   The notification is returned in the response body

   :reqjson string notificationtype: Notification type, either `service` or `host`.
   :reqjson string troublecode: Trouble code of the notification.
   :reqjson string hostname: Hostname of the host being reported on.
   :reqjson string displayname: Human-readable name of the host.
   :reqjson string servicename: For service notifications, the name of the service.
   :reqjson string state: State of the service/host.
   :reqjson string output: Output from the service host.
   :reqjson datetime icingadatetime: Timestamp of the notification from the monitoring server.

   :reqheader Authorization: required, currently only apikey allowed

   :statuscode 201: Notification created successfully
```

### Heartbeats

API endpoints for interacting with server heartbeats.

```eval_rst
.. http::post:: /heartbeat

   Create a heartbeat for the authenticated monitoring server.

   **Example response**

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: text/plain

      I'm not dead yet

   :reqjson id: ID of the notification.

   :reqheader Authorization: required, currently only apikey allowed

   :statuscode 200: Heartbeat recorded successfully.
```

### Senders

API Endpoints for interacting with sending providers.

#### Modica

API endpoints for interaction with modica.

```eval_rst
.. http:post:: /modica/mo-messages

   Callback for handling incoming SMS messages from Modica.

   :reqjson int id: Provider message ID of the incoming SMS message.
   :reqjson string source: Contact number of the sender.
   :reqjson string operator: Operator name.
   :reqjson int reply-to: Provider message ID of the SMS message the incoming one is responding to.
   :reqjson string content: Message body.
   :reqjson string encoding: Message encoding (optional, included only when the message contains binary data)
```

```eval_rst
.. http:post:: /modica/dlr-status

   Call back for handling delivery status receipts from Modica.

   :reqjson int message_id: Provider message ID of the outgoing message being reported on.
   :reqjson enum status: Message status from Modica. Possible values: 'submitted', 'sent', 'received', 'frozen', 'rejected', 'failed', 'dead', 'expired'.
```
