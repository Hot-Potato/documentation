# Trouble Code: HOTPOTATO001 (Inability to send notifications)

Since v0.5.2 Hot Potato sends a failure notification when it is unable to send a page via Modica.

By clicking the "Click here to view" link in the failed pages warning banner you will can select the failed pages and acknowledge them.

A cron job sends notifications to the person on pager and those also designated to receive failure notifications. To stop these messages you need to go into Hot Potato and Ack the failed notifications by ticking the boxes and selecting Ack.

## Contact Methods

In order for Hot Potato to send you a notification it needs to know a contact.

You can add a contact under the Account Settings via the Contact Methods section.

Click the "Add a contact method" link to add a contact method.

You should receive a test message after adding a contact method.

## Subscribing to failure notifications

Under the Contact Methods heading there is an option to enable / disable failure notifications for yourself.

When toggling this option you should get a notification.
