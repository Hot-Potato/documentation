# Trouble Code: HOTPOTATO002 (Not receiving heartbeats)

In Hot Potato v0.7.0 we added notifications for failed heartbeats.

If you get paged for these then a server has lost the ability to talk to Hot Potato and thus probably lost the ability to send pages.
