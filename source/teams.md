# Teams

Teams are a feature of Hot Potato designed to allow multiple independent groups
to monitor and respond to notifications on the same Hot Potato instance.

Each team receives only notifications directed to it, and has it's own on-call
person.

By default Hot Potato has a single team of the same name. This is the team used
for management of Hot Potato itself. A user must be a member of this team, and
have appropriate permissions, to be able to manage servers and other users &
teams.

## Escalations

Hot Potato features escalations to make sue that critical notifications are
responded to, even if the primary on-call person is unable to respond.

There are three escalation policies which determine how critical notifications
should be treated.

**Never**

This is the default policy, and disables escalations. Notifications are always
sent to the primary on-call person and no action is taken if they do not
respond.

**Outside Business Hours**

With this policy critical notifications which happen inside business hours are 
escalated if the on-call person has not read or acknowledged the notification
within a configurable delay.

Outside of business hours notifications are never sent to the original team.
Critical notifications are escalated immediately to the configured team.

**Always**

This policy is the same as 'Outside business hours' during business hours.
Critical notifications will be escalated if they are not read or acknowledged
within a configurable delay.

### Business hours

The timezone for business hours is calculated from the team's timezone. Working
day detection is provided by [workalendar](https://github.com/peopledoc/workalendar) with the region found from the timezone 
if possible. For this reason should avoid using generic timezones like UTC when
using escalations.
