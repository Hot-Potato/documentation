# Developer Guide

Contents

- [Installing](#Installing)
- [Accessing the development site](#accessing-the-development-site)
- [Pipenv commands](#pipenv-commands)
- [Generating migrations](#generating-a-new-migration-script)
- [Writing & Generating Documentation](#writing-generating-documentation)
- [Testing Provider Callbacks](#testing-provider-callbacks)
- [Adding or updating translations](#adding-or-updating-translations)

## Installing

### Prerequisites

- Python 3.6 or greater
- [pipenv](https://github.com/pypa/pipenv)
- [docker-compose](https://docs.docker.com/compose/)

### Running

To set up a development environment you first need to install the development dependencies

```bash
$ pipenv install --dev
```

Then setup the database

```bash
$ pipenv run cli db create-dev
```

Finally you can start Hot Potato with

```bash
$ docker-compose up hotpotato-dev
```

Some changes, such as those to dependencies or the docker environment require rebuilding the container:

```bash
$ docker-compose up --build hotpotato-dev
```

## Accessing the development site

Once the development site has started, the following web services are exposed to the host:

| Service           | URL                    | Default credentials                                                           |
| :---------------- | :--------------------- | :---------------------------------------------------------------------------- |
| Hot Potato        | http://localhost:8000  | `test1@example.com`/`test_password1` and `test2@example.com`/`test_password2` |
| RabbitMQ console  | http://localhost:15672 | `hotpotato`/`hotpotato`                                                       |
| Cockroach console | http://localhost:8080  | -                                                                             |

If these ports clash with other services on your system, you can override them with environment variables (see `.env` for the full list), like so:

```bash
$ COCKROACH_WEBUI_PORT=8081 docker-compose up hotpotato-dev
```

## Pipenv commands

The `Pipfile` contains a number of commands that are useful during
development.

**Running unit tests**

\$ pipenv run unit-test

**Running [Robot](https://robotframework.org/) tests**

\$ pipenv run robot-test

**Running linters**

$ pipenv run flake8
$ pipenv run black
\$ pipenv run isort

## Generating a new Migration Script

1. Bring up a Hot Potato Docker Compose instance, using a clone of `master` so that the new migration is generated against the latest revision.

2. Update the model in the code with the new changes.

3. Run the following command to generate a new revision file:

```bash
$ pipenv run cli db migrate --message "<message>"
INFO  [alembic.runtime.migration] Context impl CockroachDBImpl.
INFO  [alembic.runtime.migration] Will assume transactional DDL.
...
INFO  [alembic.autogenerate.compare] Detected added table 'message_log'
...
  Generating /code/hotpotato/migrations/versions/{date}_{time}_{revision}_{message}.py ... done
```

4. Modify the generated revision file so that it is consistent with the changes actually made. The migration script tries to remove database indexes which are in the database, but are not seen by Alembic in the database model due to a bug in the CockroachDB Alembic runtime. These changes should be removed from the `upgrade` and `downgrade` functions.

5. Add the newly generated migration script into the `/hotpotato/migrations/versions` folder of your branch, so that it gets pushed into master with the database changes.

## Writing & Generating Documentation

Documentation is generated by [sphinx](http://www.sphinx-doc.org/en/master/) and written in a mix of markdown and restructured text.

To generate the documentation run:

```bash
$ pipenv run docs
```

API documentation uses the [sphinx http module](https://sphinxcontrib-httpdomain.readthedocs.io/en/stable/) to provide structured http documentation.

## Testing Provider Callbacks

Hot Potato uses callbacks from message delivery APIs like pushover to tell if a message has been read and/or acknowledged.

If you're changing code that interacts with these it might be a good idea to manually test that they work as expected.

**Establish a Tunnel**

A simple way to temporarily make the testing application public is to use [ngrok](https://ngrok.com/).

**WARNING**: You should make sure there is no sensitive data on the instance before doing this and change the passwords of the test accounts and any others you created.

```
$ ngrok http 8000
```

Leave this command running for the duration of your testing.

**Set the Application URL**

This should give you a URL like https://12345678.ngrok.io.
Open `.env` and change the line;

```
HOTPOTATO_HOTPOTATO_WEBUI_URL=localhost:8000
```

to:

```
HOTPOTATO_HOTPOTATO_WEBUI_URL=12345678.ngrok.io

```

Note: There is no protocol here! That’s set to http whether you like it or not.

**Restart Hot Potato**

A restart is required for configuration changes to take effect.

## Adding or updating translations

Hot Potato uses [flask-babelex](https://pythonhosted.org/Flask-BabelEx/) to help translate the website into other languages.
All translating is done by people. Original strings are saved in `all_strings.pot`
Translations are kept in `translations/language/LC_MESSAGES/messages.po`
`Messages.mo` is a compiled version of `messages.po`

**To mark a string to be translated**

Add gettext around string: `gettext("my string")`
Then run: `pipenv run gen-language`

**To compile Translations**

Run: `pipenv run compile-language`

**To add Translation language files**

Run: `pipenv run pybabel init -i hotpotato/language-support/all_strings.pot -d hotpotato/app/translations -l [language code]`
Replace [language code] with a language code e.g. `en`

**Update translation language files**

Run: `pipenv run update-language`
This will update `.po` files from `all_strings.pot`
Then compile translations (see above)

Translations are stored in `app/translations`.
