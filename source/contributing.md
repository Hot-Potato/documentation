# Contributing, help and support

The Hot Potato project welcomes all contributors.

## New contributors

Hello! Welcome to Hot Potato :)

We know that making your first contribution to a project can be a little bit scary and we'd like to help with that. If you're concerned at all please feel free to reach out to us through a [confidential issue](https://docs.gitlab.com/ce/user/project/issues/confidential_issues.html) and we will help you.

## Code of Conduct

All contributors are expected to follow our code of conduct.

## How to contribute

We use GitLab issues to track all parts of the Hot Potato project. More detail on contributing in particular areas can be found below but generally the process is:

* Create an issue
* Create a branch from the issue
* Commit and push updates to the branch
* Create a Merge Request

### Convention and style guide

GitLab CI runs [black](https://github.com/psf/black), [flake8](http://flake8.pycqa.org/en/latest/), and [isort](https://pypi.org/project/isort/) on all merge requests. In addition there is an issue to supplement this with [EditorConfig](https://gitlab.com/Hot-Potato/hotpotato/issues/35).

### Bug reports

If you've found a bug in Hot Potato you can search through our issue list on gitlab.com, and if you can't find an matching report you can raise one with our bug template. We really appreciate knowing as much as we can to help track down the problem so please make sure you do your best to tell us as much as you can. If you're concerned about sharing certain log entries or information please consider using the [confidential issue](https://docs.gitlab.com/ce/user/project/issues/confidential_issues.html) feature.

### Feature requests

If there's something that would make Hot Potato even better we'd love to hear about. These can be as simple or as detailed as you'd like as long as there's enough information for us to understand what you mean.

Feature requests are prioritised based on community feedback.

### Documentation updates

It is our goal for our documentation to be accessible to everyone.

### Design

We are open to improving the design of Hot Potato to increase usability and accessibility.

### Translations

See [#5](https://gitlab.com/Hot-Potato/hotpotato/issues/5).

### Security concerns

If you have a security concern please raise a [confidential issue](https://docs.gitlab.com/ce/user/project/issues/confidential_issues.html) or email <help@hotpotato.nz>.

Please ensure you do your best to tell us:

* What the issue is
* The severity of the issue
* Where the security vulnerability is (is it in the code or our infrastructure?)
* The version of Hot Potato
* Any information about exploiting the vulnerability

If you report a security vulnerability that wasn't already known we will acknowledge your contribution to the project. It may take us a few days to respond to your issue but we will keep you posted with updates on the issue.

## Recognition

It is important that Open Source contributions are recognised. Please update the contributors file in the project as part of your commits and merge requests to include your name.
