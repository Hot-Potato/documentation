# Hot Potato

Please note that our documentation is currently a work in progress.

Hot Potato is a message broker for monitoring systems. It sits in between monitoring systems and messaging providers to ensure consistent relaying of messages to on-call staff.

Hot Potato is as a web application built on the [Flask](https://palletsprojects.com/p/flask/) framework with [CockroachDB](https://www.cockroachlabs.com/) and [RabbitMQ](https://www.rabbitmq.com/).

Monitoring systems can send notifications to the Hot Potato API and Hot Potato will pass formatted messages to on-call staff via messaging providers.

## What is Hot Potato?

Hot Potato is an open source web application intended to put on-call staff in a better position to respond to notifications. Hot Potato was originally built for internal use at [Catalyst IT](https://catalyst.net.nz/) before being released to the world.

### What isn't Hot Potato?

Hot Potato is not a monitoring system. It is not intended to replace any component of the average monitoring setup. It is intended to compliment existing systems and improve the lives of on-call staff.

### How can we trust it to work properly?

Hot Potato isn't designed to run on a single server sitting under a desk somewhere, it was built to take advantage of geographic redundancy and load balancing to ensure it is never unavailable. The example deployment section of the documentation will likely resolve any concerns you have about how to run Hot Potato safely.

### What monitoring systems are tested to work with Hot Potato?

- Nagios3
- Icinga
- Icinga2

### What notification providers can I use with Hot Potato out of the box?

- Twilio (SMS)
- Modica (SMS and NZ Pager)
- PushOver

### How easy is it to add a new monitoring system or notification provider?

This is something we'd like to support, but currently Hot Potato does not have a
stable internal API. See [this issue](https://gitlab.com/Hot-Potato/hotpotato/issues/64).

## Need help?

Take a look at our FAQ or Getting Started guide, if you can't find what you need there join the official Hot Potato IRC channel - [#hotpotato](irc://freenode/hotpotato) on FreeNode

If you find a problem with Hot Potato please see our [issue tracker](https://gitlab.com/Hot-Potato/hotpotato/issues).

## Contents

- [Code of Conduct](code-of-conduct.md)
- [Contribution guide](contributing.md)
- [Information for developers](developing.md)
- [Trouble Codes](trouble-codes.md)
- [Monitoring Systems](monitoring-systems.md)
- [Teams](teams.md)
- [APIs](api.md)
- [Upgrading](upgrading/index.md)
