# Monitoring Systems

## Notification Types

|                   | Nagios3 | Icinga 1 | Icinga 2 | Supported |
| :---------------- | :------ | :------- | :------- | :-------- |
| PROBLEM           | Yes     | Yes      | Yes      | Yes       |
| RECOVERY          | Yes     | Yes      | Yes      | Yes       |
| CUSTOM            | No      | No       | Yes      | Yes       |
| ACKNOWLEDGEMENT   | Yes     | Yes      | Yes      | No        |
| FLAPPINGSTART     | Yes     | Yes      | Yes      | No        |
| FLAPPINGSTOP      | Yes     | Yes      | No       | No        |
| FLAPPINGEND       | No      | No       | Yes      | No        |
| FLAPPINGDISABLED  | Yes     | Yes      | No       | No        |
| DOWNTIMESTART     | Yes     | Yes      | Yes      | No        |
| DOWNTIMEEND       | Yes     | Yes      | Yes      | No        |
| DOWNTIMECANCELLED | Yes     | Yes      | No       | No        |
| DOWNTIMEREMOVED   | No      | No       | Yes      | No        |

For the purposes of Hot Potato FLAPPINGSTOP and FLAPPINGEND are treated the same.

Links:

- [Nagios3][nagios3nt]
- [Icinga 1][icinga1nt]
- [Icinga 2][icinga2nt]

## Host States

|             | Nagios3 | Icinga 1 | Icinga 2 | Supported |
| :---------- | :------ | :------- | :------- | :-------- |
| UP          | Yes     | Yes      | Yes      | Yes       |
| DOWN        | Yes     | Yes      | Yes      | Yes       |
| UNREACHABLE | Yes     | Yes      | No       | Yes       |

Links:

- [Nagios3][nagios3hs]
- [Icinga 1][icinga1hs]
- [Icinga 2][icinga2hs]

## Service States

|          | Nagios3 | Icinga 1 | Icinga 2 | Supported |
| :------- | :------ | :------- | :------- | :-------- |
| OK       | Yes     | Yes      | Yes      | Yes       |
| WARNING  | Yes     | Yes      | Yes      | Yes       |
| UNKNOWN  | Yes     | Yes      | Yes      | Yes       |
| CRITICAL | Yes     | Yes      | Yes      | Yes       |

Links:

- [Nagios3][nagios3ss]
- [Icinga 1][icinga1ss]
- [Icinga 2][icinga2ss]

## Hot Potato specific

- Handover
- Alert
- Message

[nagios3nt]: https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/3/en/macrolist.html#notificationtype
[icinga1nt]: https://icinga.com/docs/icinga1/latest/en/macrolist.html#macrolist-notificationtype
[icinga2nt]: https://icinga.com/docs/icinga2/latest/doc/09-object-types/#notification
[nagios3hs]: https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/3/en/macrolist.html#hoststate
[icinga1hs]: https://icinga.com/docs/icinga1/latest/en/macrolist.html#macrolist-hoststate
[icinga2hs]: https://icinga.com/docs/icinga2/latest/doc/09-object-types/#notification
[nagios3ss]: https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/3/en/macrolist.html#servicestate
[icinga1ss]: https://icinga.com/docs/icinga1/latest/en/macrolist.html#macrolist-servicestate
[icinga2ss]: https://icinga.com/docs/icinga2/latest/doc/09-object-types/#notification
